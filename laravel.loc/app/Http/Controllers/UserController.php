<?php

namespace App\Http\Controllers;

use App\Http\Requests\EditUserProfileRequest;
use App\Models\Project;
use App\Models\Technology;
use App\Models\User;
use Helper;
use App\Services\UserTechnologyService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Services\ProjectService;
use App\Services\TechnologyService;
use App\Services\UserService;
use View;
use Mpdf\Mpdf;


class UserController extends Controller
{
    /**
     * @var ProjectService
     */
    private $projects;

    /**
     * @var TechnologyService
     */
    private $technologies;

    /**
     * @var UserService
     */
    private $user;

    public function __construct(ProjectService $projects, TechnologyService $technologies, UserService $user,
                                UserTechnologyService $user_technologies) //сюда передается нужный объект сервиса
    {
        $this->projects = $projects; //сдесь имеем доступ к сервису
        $this->technologies = $technologies;
        $this->user = $user;
        $this->user_technologies = $user_technologies;
    }

    public function index(Request $request, $id)
    {

        return view('user.index', [
            "user"         => Auth::user(),
            "projects"     => $this->projects->findById(session('project_id') ?? 0),
            "technologies" => $this->technologies->findById(session('technology_id') ?? 0)
        ]);
    }

    public function edit()
    {

        //dd(Auth::user());
        return view('user.profile', [
            "user"         => Auth::user(),
            "projects"     => $this->projects->findById(session('project_id') ?? 0),
            "technologies" => $this->technologies->findById(session('project_id') ?? 0)
        ]);
    }

    public function update(EditUserProfileRequest $request)
    {
        /** @var User $user */
        $user = Auth::user();

        $user->update($request->all());

        $user->projects()->sync($request->get('user_projects'));
        $user->technologies()->sync($request->get('user_technologies'));

        return redirect()->route('user.show');
    }

    /**

     * Find record by id

     *

     * @param int $id

     * @return Model

     */
    public function show()
    {
        return view(
            'user.show', [
            $user = Auth::user(), //метод возвращает конкретную модель по айдишнику
            $technology = $this->technologies->all()
                ]
        );
    }

    public function create()
    {
        return view('projects.index');
    }

    public function store()
    {
        $projects = new Project();

        $projects->project_name = request('project_name');
        $projects->save();

        return redirect('projects.index');



        /*$user = Auth::id();
        "name" => $request->input('name');
        $user->project_id = Project::where('project_name', $request->input('project_name'))->value('id');
        $user->save();
        return redirect()->route('projects.edit');
        //return view('projects.edit');*/


    }

    public function invoicePdf(Request $request, User $users)
    {

        /** @var User $user */
        $user = $this->user->findById(session('id') ?? 0);
        $view = View::make('user.invoicepdf',
            [
                "user" => Auth::user(),
                "projects" => $this->projects->findById(session('project_id') ?? 0),
                "technologies" => $this->technologies->findById(session('technology_id') ?? 0),

            ]
        );
        $html = $view->render();
        $mpdf = new Mpdf();
        $mpdf->WriteHTML($html);
        $mpdf->Output("invoiceUser.pdf", "D");

    }

}
