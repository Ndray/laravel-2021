<?php

namespace Database\Seeders;

use App\Models\ProjectTechnology;
use Illuminate\Database\Seeder;

class ProjectTechnologiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProjectTechnology::factory()->times(5)->create();
    }
}
