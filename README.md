For Install Project with Laravel
1) curl -s https://laravel.build/example-app
2) nvm install v14.15.3
3) npm install
4) npm run dev
5) https://github.com/mbezhanov/faker-provider-collection#integrations
6) php artisan make:auth -verify email registration
7) composer require jeroennoten/laravel-adminlte
8) php artisan adminlte:install
9) composer require laravel/ui
10) php artisan ui vue --auth
11) php artisan adminlte:install --only=auth_views
12) composer require mpdf/mpdf
