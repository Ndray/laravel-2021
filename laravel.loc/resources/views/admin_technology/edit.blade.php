@extends('adminlte::page')

@section('content')
    <form method="POST" action="{{ route('technologies.update', ["technology" => $technology->id]) }}">
        @method('PUT')
        @csrf
        <div class="row">
            <div class="col-md-12">
                <form>
                    <div class="form-group">
                        <label for="technology_name">Technology Name</label>
                        <input type="text" class="form-control @error('technology_name') is-invalid @enderror"
                               name="technology_name" id="technology_name"
                               value="{{ old('technology_name') }}">
                        @error('technology_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror


                    </div>
                </form>
            </div>
            <input type="submit" value="Save Changes" class="btn btn-info">
        </div>


@stop
