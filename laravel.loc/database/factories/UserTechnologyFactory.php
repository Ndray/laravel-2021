<?php

namespace Database\Factories;

use App\Models\Technology;
use App\Models\User;
use App\Models\UserTechnology;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserTechnologyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UserTechnology::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::all()->random()->id,
            'technology_id' => Technology::all()->random()->id
        ];
    }
}
