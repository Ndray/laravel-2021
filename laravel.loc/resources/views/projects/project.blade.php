<div class="container">
        <div class="row">
            <div class="py-8">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th scope="col">id</th>
                        <th scope="col">Project Name</th>
                        <th scope="col">Developers quantity</th>
                        <th scope="col">Created at</th>
                        <th scope="col">Project Technologies</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">{{$project->id}}</th>
                        <td>{{$project->project_name}}</td>
                        <td>{{$project->developers_quantity}}</td>
                        <td>{{$project->created_at}}</td>
                        <td> @foreach ($project->technologies as $technology)
                                 {{ $technology->technology_name }}
                            @endforeach </td>

                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
</div>

