<?php

namespace App\Http\Controllers;

use App\Models\Technology;
use Illuminate\Http\Request;
use App\Services\TechnologyService;
use App\Http\Requests\TechnologyStoreRequest;
use App\Http\Requests\TechnologyUpdateRequest;

class AdminTechnologyController extends Controller
{
    public function __construct(TechnologyService $technologies) //сюда передается нужный объект сервиса
    {
        $this->technologies = $technologies; //сдесь имеем доступ к сервису
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() // метод для отображения данных всех проектов
    {
        return view('admin_technology.index',
            [
                "technologies" => $this->technologies->all(),
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()  // метод для создания
    {
        return view('admin_technology.create',
            [
                "technologies" => $this->technologies->all()

            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(TechnologyStoreRequest $request) // метод для сохранения нового проекта
    {
        $technology = $this->technologies->create($request->all());
        return redirect()->route('technologies.show', ["technology" => $technology->id])->withSuccess('Technology #' . $technology->id . 'was created!');;
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) // этот метод вывод конкретную модель продукта  по айдишнику из метода findById
    {
        return view('admin_technology.show',
            ["technology" => $this->technologies->findById($id)] //метод возвращает конкретную модель по айдишнику
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) // показать форму редактирования
    {

        return view('admin_technology.edit',
            [
                "technology" => $this->technologies->findById($id), //метод возвращает конкретную модель по айдишнику
                //"technology" => $this->technologies->all()
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(TechnologyUpdateRequest $request, $id) // обновить отредактированый
    {
        //dd(11);
        $this->technologies->update($id, $request->except(['_method', '_token']));
        return redirect()->route('technologies.show', ["technology" => $id])->withSuccess('Technology #' . $id . 'was updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->technologies->destroy($id);
        return redirect()->route('technologies.index')->withSuccess('Technologies #' . $id . 'was deleted!');
    }



}
