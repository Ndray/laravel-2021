@extends('adminlte::page')

@section('content')
    <form method="POST" action="{{ route('technologies.store') }}">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <form>
                    <div class="form-group">
                        <label for="technology_name">Create Technology</label>
                        <input type="text" class="form-control @error('technology_name') is-invalid @enderror"
                               name="technology_name" id="technology_name" value="{{ old('technology_name') }}">
                        @error('technology_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror

                    </div>
                    <button type="submit" onclick="create()" class="btn btn-info">Create Technology</button>
                </form>



@endsection


