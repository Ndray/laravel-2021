<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\Technology;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Http\Request;
use App\Services\ProjectService;
use App\Services\ProjectTechnologyService;
use App\Services\TechnologyService;

class ProjectController extends Controller
{
    /**
     * @var ProjectService
     */
    private $projects;

    /**
     * @var ProjectTechnologyService
     */
    private $projectTechnology;

    /**
     * @var TechnologyService
     */
    private $technologies;

    /**
     * @var User
     */

    public function __construct(
        ProjectService $projects,
        ProjectTechnologyService $projectTechnology,
        TechnologyService $technologies
    ) {
        $this->projects = $projects;
        $this->projectTechnology = $projectTechnology;
        $this->technologies = $technologies;
    }

    public function index(Request $request)
    {
//        $users = User::with('projects', 'technologies')->get();

        $projects = Project::with('users', 'technologies')->get();

        $technologies = Technology::with('users', 'projects')->get();

//        dd($users, $projects, $technologies);

        return view('projects.index', ["projects" => $projects]);
    }

    public function create()
    {
        return view('projects.index');
    }

    public function store()
    {
        $projects = new Project();

        $projects->project_name = request('project_name');
        $projects->save();

        return redirect('projects.index');
    }

}
