<?php


namespace App\Repositories;
use App\Repositories\BaseRepository;
use App\Models\ProjectTechnology;


class ProjectTechnologyRepository extends BaseRepository
{
    public function __construct(ProjectTechnology $model) // модель класса юзер
    {
        $this->model = $model; // в конструктор репозитория бу передаваться модель юзера
    }

}
