<?php


namespace App\Services;

use App\Services\BaseService;
use App\Repositories\UserTechnologyRepository;


class UserTechnologyService extends BaseService
{
    public function __construct(UserTechnologyRepository $repo)
    {
        $this->repo = $repo;
    }
}
