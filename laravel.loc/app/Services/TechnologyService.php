<?php


namespace App\Services;

use App\Services\BaseService;
use App\Repositories\TechnologyRepository;


class TechnologyService extends BaseService
{
    public function __construct(TechnologyRepository $repo)
    {
        $this->repo = $repo;
    }
}
