<?php


namespace App\Repositories;
use App\Repositories\BaseRepository;
use App\Models\UserTechnology;



class UserTechnologyRepository extends BaseRepository
{
    public function __construct(UserTechnology $model) // модель класса юзер
    {
        $this->model = $model; // в конструктор репозитория бу передаваться модель юзера
    }
}
