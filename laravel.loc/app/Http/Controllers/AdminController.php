<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ProjectService;
use App\Services\UserService;
use App\Services\TechnologyService;

class AdminController extends Controller
{
    public function __construct(ProjectService $projects, UserService $users, TechnologyService $technologies)
    {
        $this->projects = $projects;
        $this->users = $users;
        $this->technologies = $technologies;
    }

    public function index(Request $request)
    {

        return view('admin.admin_main',
            [
                "projectsCount" => $this->projects->count(),
                "technologiesCount" => $this->technologies->count(),
                "usersCount" => $this->users->activeUsersCount(),
            ]
        );
    }




}
