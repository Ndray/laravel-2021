<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ProjectService;
use App\Services\TechnologyService;
use App\Models\Project;
use App\Http\Requests\ProjectStoreRequest;
use App\Http\Requests\ProjectUpdateRequest;



class AdminProjectController extends Controller
{
    /**
     * @var ProjectService
     */
    private $projects;

    /**
     * @var TechnologyService
     */
    private $technologies;

    public function __construct(ProjectService $projects, TechnologyService $technologies) //сюда передается нужный объект сервиса
    {
        $this->projects = $projects; //сдесь имеем доступ к сервису
        $this->technologies = $technologies;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index() // метод для отображения данных всех проектов
    {
        return view('admin_project.index',
            ["projects" => $this->projects->all(),
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()  // метод для создания
    {

        return view('admin_project.create',
            [
                "technologies" => $this->technologies->all()
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectStoreRequest $request) // метод для сохранения нового проекта
    {
        /** @var Project $project */

        $project = $this->projects->create($request->all());
        $project->technologies()->sync($request->input('technology_id'));
        //$project->technologies()->sync($request->get('technology_id'));

        return redirect()->route('projects.show', ["project" => $project->id])->withSuccess('Project #' . $project->id . 'was created!');;
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function show($id) // этот метод вывод конкретную модель продукта  по айдишнику из метода findById
    {
        return view('admin_project.show',
            ["project"    => $this->projects->findById($id),//метод возвращает конкретную модель по айдишнику
                "technology" => $this->technologies->all()
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit($id) // показать форму редактирования
    {

        return view('admin_project.edit',
            [
                "project"      => $this->projects->findById($id), //метод возвращает конкретную модель по айдишнику
                "technologies" => $this->technologies->all()
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectUpdateRequest $request, $id) // обновить отредактированый
    {
//        dd($request->all());

        /** @var Project $project */
        $project = Project::query()->find($id);

        $project->update($request->except(['_method', '_token']));
        $project->technologies()->sync($request->get('technology'));

        return redirect()->route('projects.show', ["project" => $id])->withSuccess('Project #' . $id . 'was updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->projects->destroy($id);
        return redirect()->route('projects.index')->withSuccess('Project #' . $id . 'was deleted!');
    }
}

