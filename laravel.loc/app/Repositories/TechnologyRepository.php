<?php


namespace App\Repositories;
use App\Repositories\BaseRepository;
use App\Models\Technology;


class TechnologyRepository extends BaseRepository
{
    public function __construct(Technology $model) // модель класса юзер
    {
        $this->model = $model; // в конструктор репозитория бу передаваться модель юзера
    }
}
