<?php


namespace App\Services;

use App\Services\BaseService;
use App\Repositories\ProjectTechnologyRepository;



class ProjectTechnologyService extends BaseService
{
    public function __construct(ProjectTechnologyRepository $repo)
    {
        $this->repo = $repo;
    }
}
