@extends('adminlte::page')

@section('content')
    <div class="row">
        @include('partials.alerts')
        <div class="col-md-12">
        <div class="box-body">
            <p> Technology name is <br>
            <strong><i class="fa fa-book margin-r-5"></i> {{ $technology->technology_name }} </strong>
            </p>

            <p class="text-muted">
               Technology ID is # {{ $technology->id }}
            </p>

            <hr>

            <strong><i class="fa fa-pencil margin-r-5"></i> Created at </strong>

            <p> {{ $technology->created_at }} </p>
            <hr>


            <strong><i class="fa fa-pencil margin-r-5"></i> Updatet at</strong>


            <p>  {{ $technology->updated_at }} </p>

            <hr>

            <strong><i class="fa fa-file-text-o margin-r-5"></i> Description</strong>

            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
        </div>
            <a href="{{ route('technologies.edit', ["technology" => $technology->id]) }}" type="button" class="btn btn-info">Edit</a>
        </div>




@stop
