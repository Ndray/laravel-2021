<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditUserProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()             //здесь задаем правила,которые хотим использовать для валидации
    {
        return
            [
                'name' => 'required|max:255|min:3',
                'user_project' => 'alpha|max 30|exists:project_name',
                'user_technology' => 'alpha|max 30|exists:technology_name'
            ];

    }
}
