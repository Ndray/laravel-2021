@extends('adminlte::page')

@section('content')
    @include('partials.alerts')
    <table id="technologies" class="table table-bordered table-hover dataTable" role="grid"
           aria-describedby="example2_info">
        <thead>
        <tr role="row">
            <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending"
            >Technology ID
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
            >Technology Name
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
            >Created at
            </th>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
            >Updated at
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
            >Actions
        </tr>
        </thead>
        <tbody>
        @foreach($technologies as $technology)
            <tr role="row">
                <td>{{ $technology->id }}</td>
                <td>{{ $technology->technology_name }}</td>
                <td>{{ $technology->created_at }}</td>
                <td>{{ $technology->updated_at }}</td>
                <td>
                    <div class="btn-group">
                        <a href="{{ route('technologies.show', ["technology" => $technology->id]) }}" type="button"
                           class="btn btn-info">View</a>
                        <a href="{{ route('technologies.edit', ["technology" => $technology->id]) }}" type="button"
                           class="btn btn-info">Edit</a>
                        <form method="POST" action="{{ route('technologies.destroy', ["technology" => $technology->id]) }}">
                            @method('DELETE')
                            @csrf
                            <input type="submit" value="delete"  class="btn btn-info">
                        </form>
                    </div>
                </td>
        @endforeach
        </tbody>
    </table>

@stop
