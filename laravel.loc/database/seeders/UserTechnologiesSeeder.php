<?php

namespace Database\Seeders;

use App\Models\UserTechnology;
use Illuminate\Database\Seeder;

class UserTechnologiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserTechnology::factory()->times(5)->create();
    }
}
