@extends('layouts.main')

@section('content')
    <h1>User</h1>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        !!!
                        {{ __('You are logged in!') }}

                        <main role="main">
    @foreach($projects as $project)
        @include('projects.project', ['project' => $project])
    @endforeach






@endsection



